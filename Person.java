import java.util.Objects;

/**
 * Project GUI Basics
 * Author Nick Tagliamonte
 * Created 2/23
 * Description
 * The Person class is just a simple class containing fields for first name, last name, age, birth month, and birth year.
 * It only contains setters and getters for each field and an equals, toString, and hashCode method.
 */

public class Person {
	public String firstName;
	public String lastName;
	public int age;
	public String birthMonth;
	public int birthYear;
	
	public Person(String firstName, String lastName, int age, String birthMonth, int birthYear) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
		this.birthMonth = birthMonth;
		this.birthYear = birthYear;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getBirthMonth() {
		return birthMonth;
	}

	public void setBirthMonth(String birthMonth) {
		this.birthMonth = birthMonth;
	}

	public int getBirthYear() {
		return birthYear;
	}

	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
	}

	@Override
	public String toString() {
		return "Person [firstName=" + firstName + ", lastName=" + lastName + ", age=" + age + ", birthMonth="
				+ birthMonth + ", birthYear=" + birthYear + "]";
	}

	@Override
	public int hashCode() {
		return Objects.hash(age, birthMonth, birthYear, firstName, lastName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		return age == other.age && Objects.equals(birthMonth, other.birthMonth) && birthYear == other.birthYear
				&& Objects.equals(firstName, other.firstName) && Objects.equals(lastName, other.lastName);
	}
	
	
}
