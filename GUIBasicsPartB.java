import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.Box;
import java.awt.BorderLayout;
import java.awt.Dimension;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

/**
 * Project GUI Basics
 * Author Nick Tagliamonte
 * Created 2/23
 * Description
 * The GUIBasicsPartB class creates a form with fields for a first name, last name, age, birth month, and birth year.
 * It uses the information input into these fields to create a person object.
 * That object is added to a list of people which is displayed in the GUI window.
 * The details of individual people from the list can be printed by double clicking them
 * There are also buttons to add a new person to the list, print the details of all the people in the list, or clear the list.
 */

public class GUIBasicsPartB {

	private JFrame frame;
	private JTextField firstNameField;
	private JTextField lastNameField;
	private JTextField ageField;
	
	//This list of person objects is declared here so that it is accessable from multiple methods
	public List<Person> personList = new ArrayList<Person>();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUIBasicsPartB window = new GUIBasicsPartB();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUIBasicsPartB() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void initialize() {
		//Create the GUI window
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//The outermost box to hold the buttons below the user input fields
		Box outerBox = Box.createVerticalBox();
		frame.getContentPane().add(outerBox, BorderLayout.CENTER);
		
		//Create a horizontal box to hold the user input fields and user list arranged horizontally
		Box labelAndListHBox = Box.createHorizontalBox();
		outerBox.add(labelAndListHBox);
		
		//Create a vertical box to stack each of the various user input fields within the box above
		Box LabelsVBox = Box.createVerticalBox();
		LabelsVBox.setBorder(new EmptyBorder(50, 0, 20, 0));
		labelAndListHBox.add(LabelsVBox);
		
		//Horizontal box to hold the First Name field and First name label next to each other
		//The createRigidArea section of this block is for horizontal spacing 
		//This is repeated with the same naming convention for last name, age, month, and year
		Box firstNameBox = Box.createHorizontalBox();
		firstNameBox.setBorder(new EmptyBorder(0, 0, 5, 5));
		LabelsVBox.add(firstNameBox);
		firstNameBox.add(Box.createRigidArea(new Dimension(5,0)));
		
		//Create the label for the user to enter the first name
		//This is repeated with the same naming convention for last name, age, month, and year
		JLabel firstNameLabel = new JLabel("First Name: ");
		firstNameBox.add(firstNameLabel);
		firstNameBox.add(Box.createRigidArea(new Dimension(13,0)));
		
		//Create a field for the user to enter the first name
		//This is repeated with the same naming convention for last name, age, month, and year
		firstNameField = new JTextField();
		firstNameField.setMaximumSize(new Dimension(2147483647, 23));
		firstNameField.setToolTipText("Enter First Name");
		firstNameBox.add(firstNameField);
		
		Box lastNameBox = Box.createHorizontalBox();
		lastNameBox.setBorder(new EmptyBorder(0, 0, 5, 5));
		LabelsVBox.add(lastNameBox);
		lastNameBox.add(Box.createRigidArea(new Dimension(5,0)));
		
		JLabel lastNameLabel = new JLabel("Last Name: ");
		lastNameBox.add(lastNameLabel);
		lastNameBox.add(Box.createRigidArea(new Dimension(13,0)));
		
		lastNameField = new JTextField();
		lastNameField.setMaximumSize(new Dimension(2147483647, 23));
		lastNameField.setToolTipText("Enter Last Name");
		lastNameBox.add(lastNameField);
		lastNameField.setColumns(10);
		
		Box ageBox = Box.createHorizontalBox();
		ageBox.setBorder(new EmptyBorder(0, 0, 5, 5));
		LabelsVBox.add(ageBox);
		ageBox.add(Box.createRigidArea(new Dimension(5,0)));
		
		JLabel ageLabel = new JLabel("Age: ");
		ageBox.add(ageLabel);
		ageBox.add(Box.createRigidArea(new Dimension(51,0)));
		
		ageField = new JTextField();
		ageField.setMaximumSize(new Dimension(2147483647, 23));
		ageField.setToolTipText("Enter Age");
		ageBox.add(ageField);
		ageField.setColumns(10);
		
		Box monthBox = Box.createHorizontalBox();
		monthBox.setBorder(new EmptyBorder(0, 0, 5, 5));
		LabelsVBox.add(monthBox);
		monthBox.add(Box.createRigidArea(new Dimension(5,0)));
		
		JLabel monthLabel = new JLabel("Select Month: ");
		monthBox.add(monthLabel);
		
		//This creates a dropdown with a list of months for the user to select from
		JComboBox monthComboBox = new JComboBox();
		monthComboBox.setMaximumSize(new Dimension(32767, 23));
		monthComboBox.setToolTipText("Select Birst Month");
		monthComboBox.setModel(new DefaultComboBoxModel(new String[] {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}));
		monthComboBox.setSelectedIndex(0);
		monthBox.add(monthComboBox);
		
		Box yearBox = Box.createHorizontalBox();
		yearBox.setBorder(new EmptyBorder(0, 0, 5, 5));
		LabelsVBox.add(yearBox);
		yearBox.add(Box.createRigidArea(new Dimension(5,0)));
		
		JLabel yearLabel = new JLabel("Select Year: ");
		yearBox.add(yearLabel);
		yearBox.add(Box.createRigidArea(new Dimension(9,0)));
		
		////This creates a dropdown with a list of Years for the user to select from
		JComboBox yearComboBox = new JComboBox();
		yearComboBox.setMaximumSize(new Dimension(32767, 23));
		yearComboBox.setToolTipText("Select Birth Year");
		yearComboBox.setModel(new DefaultComboBoxModel(new String[] {"1923", "1924", "1925", "1926", "1927", "1928", "1929", "1930", "1931", "1932", 
				"1933", "1934", "1935", "1936", "1937", "1938", "1939", "1940", "1941", "1942", 
				"1943", "1944", "1945", "1946", "1947", "1948", "1949", "1950", "1951", "1952", 
				"1953", "1954", "1955", "1956", "1957", "1958", "1959", "1960", "1961", "1962", 
				"1963", "1964", "1965", "1966", "1967", "1968", "1969", "1970", "1971", "1972", 
				"1973", "1974", "1975", "1976", "1977", "1978", "1979", "1980", "1981", "1982", 
				"1983", "1984", "1985", "1986", "1987", "1988", "1989", "1990", "1991", "1992", 
				"1993", "1994", "1995", "1996", "1997", "1998", "1999", "2000", "2001", "2002", 
				"2003", "2004", "2005", "2006", "2007", "2008", "2009", "2010", "2011", "2012", 
				"2013", "2014", "2015", "2016", "2017", "2018", "2019", "2020", "2021", "2022", "2023"}));
		yearComboBox.setSelectedIndex(70);
		yearBox.add(yearComboBox);
		
		//This creates the user list and an action listener for if a person in the list is double clicked
		//Then that person's toString method is called and printed
		DefaultListModel model = new DefaultListModel();
		JList list = new JList(model);
		list.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e)
			{
			  if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON1) {
			    int index = list.getSelectedIndex();
			    System.out.println(personList.get(index).toString());
			  }
			}
		});
		labelAndListHBox.add(list);
		
		//This box holds the 3 buttons next to one another
		Box horizontalBox_1 = Box.createHorizontalBox();
		outerBox.add(horizontalBox_1);
		
		//This creates a button to add a person to the list based on the information that was entered in the text fields
		//Once the button is pressed and a new person is created, the text fields are cleared
		JButton addButton = new JButton("Add to List");
		addButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				addPerson(firstNameField.getText(), lastNameField.getText(), Integer.parseInt(ageField.getText()), monthComboBox.getSelectedItem().toString(), Integer.parseInt(yearComboBox.getSelectedItem().toString()));
				
				model.insertElementAt(firstNameField.getText() + " " + lastNameField.getText(), model.getSize());
				
				firstNameField.setText(null);
				lastNameField.setText(null);
				ageField.setText(null);
			}
		});
		horizontalBox_1.add(addButton);
		
		//This creates a button and an action listener that iterataes through the list of people when pressed and prints the toString for each person in the list
		JButton printButton = new JButton("Print List");
		printButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				for (int i = 0; i < personList.size(); i++) {
					System.out.println(personList.get(i).toString());
				}
			}
		});
		horizontalBox_1.add(printButton);
		
		//This creates a button and action listener to empty the list 
		JButton clearButton = new JButton("Clear List");
		clearButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				model.removeAllElements();
				personList.removeAll(personList);
			}
		});
		horizontalBox_1.add(clearButton);
	}

	/**
	 * This method creates a new person object with the data passed in from the user input fields, and adds that person to the personList
	 * @param text
	 * @param text2
	 * @param parseInt
	 * @param string
	 * @param parseInt2
	 */
	protected void addPerson(String text, String text2, int parseInt, String string, int parseInt2) {
		Person newPerson = new Person(text, text2, parseInt, string, parseInt2);
		personList.add(newPerson);		
	}
}