import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

/**
 * Project GUI Basics
 * Author Nick Tagliamonte
 * Created 2/23
 * Description
 * The GUIBasicsPartA class creates a form with the title 'Java GUI' and 2 buttons.  
 * The first causes Hello World to appear in a label and the second causes Hello World from a Java GUI to appear in that same label.
 */

public class GUIBasicsPartA {

	private JFrame frmJavaGui;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				try {
					GUIBasicsPartA window = new GUIBasicsPartA();
					window.frmJavaGui.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public GUIBasicsPartA() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		//sets the conditions for the GUI Window and establishes a title
		frmJavaGui = new JFrame();
		frmJavaGui.setTitle("Java GUI");
		frmJavaGui.setBounds(100, 100, 300, 125);
		frmJavaGui.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

		//create the label which will respond to button presses, add it to the form
		JLabel lblNewLabel = new JLabel(" ");
		lblNewLabel.setHorizontalAlignment(SwingConstants.CENTER);
		frmJavaGui.getContentPane().add(lblNewLabel, BorderLayout.NORTH);

		//Create a panel to add content to
		JPanel panel = new JPanel();
		frmJavaGui.getContentPane().add(panel, BorderLayout.CENTER);

		//Create the first button and create an action listener to change the label text on a mouse click
		JButton btnNewButton = new JButton("Button 1");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblNewLabel.setText("Hello World");
			}
		});
		panel.add(btnNewButton);

		//Create the second button and create an action listener to change the label text on a mouse click
		JButton btnNewButton_1 = new JButton("Button 2");
		btnNewButton_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				lblNewLabel.setText("Hello World from a Java GUI");
			}
		});
		panel.add(btnNewButton_1);
	}
}